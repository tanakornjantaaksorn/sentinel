import os, glob, logging

all_url_list = []


def Create_directory():
    sumary_size = []
    for url in all_url_list:
        split_url = str(url).split('/')
        path_sum = '/tmp/'
        for i in range(4, 11):
            try:
                if i == 4:
                    path_sum = path_sum + split_url[i]
                    os.mkdir(path_sum)
                    print('test......' + path_sum)
                elif i == 8:
                    path_sum = path_sum + '/' + split_url[8].split('_')[2][0:4]
                    print(path_sum)
                    os.mkdir(path_sum)
                    # print(splitpath_sentinel[8].split('_')[2][0:4])
                elif i == 9:
                    path_sum = path_sum + '/' + split_url[8].split('_')[2][4:6]
                    print(path_sum)
                    os.mkdir(path_sum)

                elif i == 10:
                    path_sum = path_sum + '/' + split_url[8].split('_')[2][6:8]

                    os.mkdir(path_sum)

                    print('wait.. Download')
                    print('path sum:',path_sum)
                    # os.system('gsutil -m cp -r {}/* ./{}'.format(url, path_sum))
                    size_download = get_size(path_sum)
                    sumary_size.append(size_download)
                    print('success download')

                    logging.basicConfig(filename='log/Download.log', format='%(asctime)s %(message)s')
                    logging.warning('Download..file {} | size: {}'.format(url, size_download))

                else:
                    path_sum = path_sum + '/' + split_url[i]
                    print(path_sum)
                    os.mkdir(path_sum)

            except FileExistsError:
                pass

    sum_download_list = sum(sumary_size)
    print(sum_download_list)
    total_size_download = format_bytes(sum_download_list)
    print('Total size download: {}'.format(total_size_download))

def format_bytes(size):
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0: '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    to_return = '{:.2f} {}B'.format(size, power_labels[n])
    return to_return


def get_size(start_path):
    total_size = 0
    for dirpath, dirnames, filenames in os.walk(start_path):
        for f in filenames:
            fp = os.path.join(dirpath, f)
            # skip if it is symbolic link
            if not os.path.islink(fp):
                total_size += os.path.getsize(fp)

    return total_size


def Create_URL_Filter(filter):
    listuri = glob.glob("URI/{}.csv".format(filter))
    for list_csv in listuri:
        file_uri = open('{}'.format(list_csv), 'r+')
        for uri in file_uri:
            split_url = uri.split(',')
            url_insert = split_url[0].split('/')

            '''insert L2 in path'''
            url_insert.insert(3, 'L2')

            '''replace L1C to L2A and add path'''
            replace_L1 = url_insert[8].replace('L1C', 'L2A').split('_')
            replace_L1.insert(3, '*')
            L2A_sum = '_'.join(replace_L1[0:4])

            url_insert.insert(8, L2A_sum)

            url_insert.insert(9, 'GRANULE')
            url_insert.insert(10, '*')
            # url_insert.insert(10, split_url[4].replace('L1C', 'L2A').replace('\n', ''))

            url_insert.insert(11, 'IMG_DATA')
            url_insert.remove(url_insert[-1])

            list_url = '/'.join(url_insert)
            all_url_list.append(list_url)

            print(list_url)



def main():
    while True:
        print('''
==== Download Sentinel L2A ====
1. Download all file
2. Download specify file
q. quit program download L2A
''')
        select_number = str(input('Enter number: '))
        if select_number == '1':
            Create_URL_Filter(filter='L2A_*')
            Create_directory()
        elif select_number == '2':
            print('''
Example:
Download L2A, year 2020: L2A_*_2020*
''')
            filter = input('Enter regular expression: ')
            Create_URL_Filter(filter)
            Create_directory()
        elif select_number == 'q':
            break





# print(all_url_list)


# print(path_sum)
#
# print('wait.. Download')
# os.system('gsutil -m cp -r gs://gcp-public-data-sentinel-2/L2/tiles/47/P/*/S2A_MSIL2A_2020*/GRANULE/*/IMG_DATA/* ./test')
# print('success download')


