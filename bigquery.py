import os, glob, logging
from google.cloud import bigquery


os.environ["GOOGLE_APPLICATION_CREDENTIALS"]="sentinel-280008-cfaa1003076d.json"  # Credentials Service Account
client = bigquery.Client()  # Start the BigQuery Client


def query_data_uri():  # test function
    dic_query = {
        'dataframe_L2A': "SELECT base_url, mgrs_tile, total_size, sensing_time, granule_id FROM `bigquery-public-data.cloud_storage_geo_index.sentinel_2_index` WHERE mgrs_tile = '47PPR' and lower(sensing_time) LIKE '2020%'",
        'dataframe_Landsat': "SELECT base_url, scene_id, total_size, date_acquired FROM `bigquery-public-data.cloud_storage_geo_index.landsat_index` WHERE upper(scene_id) LIKE 'LC8233193%' and lower(date_acquired) LIKE '2019%'"}

    for dataframe, valuequery in dic_query.items():
        query_job = client.query(valuequery)    # Start Query API Request
        query_result = query_job.result()  # Get Query Result
        # print(query_result)
        df = query_result.to_dataframe()   # Save the Query Result Dataframe
        df.to_csv(r'URI/{}.csv'.format(dataframe), index=False, header=False)  # export df to csv file
        print(df)


def query_uri(sentinel, mgrs_id, select_year):  # function query uri L2A and Landsat8
    if sentinel == 'L2A':
        Query = "SELECT base_url, mgrs_tile, total_size, sensing_time, granule_id FROM `bigquery-public-data.cloud_storage_geo_index.sentinel_2_index` WHERE mgrs_tile = '{}' and lower(sensing_time) LIKE '{}%'".format(mgrs_id, select_year)
    else:
        Query = "SELECT base_url, scene_id, total_size, date_acquired FROM `bigquery-public-data.cloud_storage_geo_index.landsat_index` WHERE upper(scene_id) LIKE 'LC8{}%' and lower(date_acquired) LIKE '{}%'".format(mgrs_id, select_year)
    query_job = client.query(Query)  # Start Query API Request
    query_result = query_job.result()  # Get Query Result
    df = query_result.to_dataframe()  # Save the Query Result Dataframe
    df.to_csv(r'URI/{}_{}_{}.csv'.format(sentinel, mgrs_id, select_year), index=False, header=False)  # convert Dataframe to CSV file save in path URI/*
    print(df)
    logging.basicConfig(filename='log/bigquery.log', format='%(asctime)s %(message)s')
    logging.warning("Query output: URI/{}_{}_{}.csv".format(sentinel, mgrs_id, select_year))


def query_uri_infile(date_query):  # function query in file
    list_mgrs = open('list-mgrs-thai', 'r+')  # open file list mgrs
    for mgrs in list_mgrs:  # Loop in file list-mgrs-thai
        Query = "SELECT base_url, mgrs_tile, total_size, sensing_time, granule_id FROM `bigquery-public-data.cloud_storage_geo_index.sentinel_2_index` WHERE mgrs_tile = '{}' and lower(sensing_time) LIKE '{}%'".format(mgrs.split('\n')[0], date_query)
        query_job = client.query(Query)
        query_result = query_job.result()
        df = query_result.to_dataframe()
        df.to_csv(r'URI/L2A_{}_{}.csv'.format(mgrs.split('\n')[0], date_query), index=False, header=False)
        print(df)
        logging.basicConfig(filename='log/bigquery.log', format='%(asctime)s %(message)s')
        logging.warning("Query output: URI/L2A_{}_{}.csv".format(mgrs.split('\n')[0], date_query))


def loop_inURI(filter_uri):  # function summary file size
    all_list = []
    dic_sum = {}
    listuri = glob.glob("URI/{}.csv".format(filter_uri))

    for uri in listuri: # Loop in Directory find
        sum_list = []
        name = uri.split('/')[1]
        filex = open('{}'.format(uri), 'r+')
        for i in filex: # Loop in file .csv
            x = i.split(',')
            sum_list.append(int(x[2]))
            all_list.append(int(x[2]))
        total = format_bytes(sum(sum_list))
        dic_sum.update({name: total})
        print(dic_sum)

    print('Summary file:',len(dic_sum))
    all_total = format_bytes(sum(all_list))
    print('Total: {}'.format(all_total))


def format_bytes(size):
    # 2**10 = 1024
    power = 2**10
    n = 0
    power_labels = {0 : '', 1: 'K', 2: 'M', 3: 'G', 4: 'T'}
    while size > power:
        size /= power
        n += 1
    to_return = '{:.2f} {}B'.format(size, power_labels[n])
    return to_return


def main():
    while True:
        print('''
==== Select Query Data and URI L2A OR Landsat ====
1. L2A
2. Landsat8
3. L2A in file
4. Summary file URI
q. quit program Query Data''')
        select_querydata = str(input('Input number for query data: '))
        if select_querydata == '1':
            print('Example MGRS: 47PPR')
            mgrs = input('Enter MGRS (Military Grid Reference System): ')
            year = str(input('Enter year: '))
            query_uri(sentinel='L2A', mgrs_id=mgrs, select_year=year)

        elif select_querydata == '2':
            print('Example MGRS: 233193')
            mgrs = input('Enter MGRS (Military Grid Reference System): ')
            year = str(input('Enter year: '))
            query_uri(sentinel='Landsat8', mgrs_id=mgrs, select_year=year)

        elif select_querydata == '3':
            print('''
=== Query in file list-mgrs-thai ===
    |Query year: 2020           |
    |Query month: 2020-06       |
    |Query full date: 2020-06-08|''')
            date_query = str(input('Enter date to search: '))
            query_uri_infile(date_query)

        elif select_querydata == '4':
            print('''
=== Summary file URI ===
Example
filter month: L2A_*_2020_06*
''')
            filter_uri = str(input('Enter regular expression: '))
            loop_inURI(filter_uri)
        elif select_querydata == 'q':
            break

