import os, logging, glob

all_url_list = []


def Create_URL_Filter(filter):
    listuri = glob.glob("URI/{}.csv".format(filter))
    for list_csv in listuri:
        csv_file = open('{}'.format(list_csv), 'r+')
        for uri_list in csv_file:
            split_url = uri_list.split(',')
            all_url_list.append(split_url[0])


def Create_directory():
    path_sum = ''
    for url in all_url_list:
        split_url = str(url).split('/')
        for i in range(3, 10):

            try:
                if i == 3:
                    path_sum = split_url[i]
                    os.mkdir(split_url[i])
                    print(path_sum)

                elif i == 7:
                    path_sum = path_sum + '/' + split_url[7].split('_')[3][0:4]
                    print(path_sum)
                    os.mkdir(path_sum)
                elif i == 8:
                    path_sum = path_sum + '/' + split_url[7].split('_')[3][4:6]
                    print(path_sum)
                    os.mkdir(path_sum)
                elif i == 9:
                    path_sum = path_sum + '/' + split_url[7].split('_')[3][6:8]
                    print('f: ' + path_sum)
                    os.mkdir(path_sum)
                    print('wait.. Download')
                    os.system('gsutil -m cp -r {}/* ./{}'.format(url, path_sum))
                    print('success download')

                    '''logging'''
                    print('test...... {}'.format(url))
                    logging.basicConfig(filename='log/Download.log', format='%(asctime)s %(message)s')
                    logging.warning("Download file Landsat8: {}".format(url))

                else:
                    path_sum = path_sum + '/' + split_url[i]
                    print(path_sum)
                    os.mkdir(path_sum)

            except FileExistsError:
                pass



def main():
    while True:
        print('''
==== Download Sentinel Landsat8 ====
1. Download all file
2. Download specify file
q. quit program download Landsat8
''')
        select_number = str(input('Enter number: '))
        if select_number == '1':
            Create_URL_Filter(filter='Landsat8_*')
            Create_directory()
        elif select_number == '2':
            print('''
Example:
Download Landsat8, year 2020: Landsat8_*_2020*
''')
            filter = input('Enter regular expression: ')
            Create_URL_Filter(filter)
            Create_directory()
        elif select_number == 'q':
            break


