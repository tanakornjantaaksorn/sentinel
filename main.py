
while True:
    print('''
==== Download sentinel and landsat8 on GCP ====
1. Query data and summary data
2. Download data L2A
3. Download data Landsat8
q. quit main program
''')
    select_number = str(input('Enter number: '))
    if select_number == '1':
        from bigquery import *
        main()
    elif select_number == '2':
        from gsutil_download_L2A import *
        main()
    elif select_number == '3':
        from gsutil_download_landsat import *
        main()
    elif select_number == 'q':
        break
